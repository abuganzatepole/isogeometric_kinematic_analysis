/* fit spline surface patch to porcine skin */

/* to compile and generate the executable.
- Include directories:
	/Path_to_SISL/SISL-master/include	
	/Path_to_BOOST/include
	/Path_to_SISL/SISL-master/examples/streaming/include
SISL libraries:
	/Path_to_SISL/SISL-master/SISL_build/libsisl.a
Source code for writing output files (in addition to the source code of this program)
	/Path_to_SISL/SISL-master/examples/streaming/src/GoReadWrite.cpp

compile this, for example:
g++ -I/Path_to_SISL/SISL-master/include/ -I/Path_to_BOOST/include -I/Path_to_SISL/SISL-master/examples/streaming/include -L/Path_to_SISL/SISL-master/SISL_build/ -lsisl generateCurve.cpp /Path_to_SISL/SISL-master/examples/streaming/src/GoReadWrite.cpp -o generateCurve
*/

#include "sisl.h"
#include "GoReadWrite.h"

#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <string>
#include <vector>

int main(int argc, char *argv[])
{

double points[363];
int im1=11;
int im2=11;
int idim=3;
double par1[]={0,1,2,3,4,5,6,7,8,9,10};
double par2[]={0,1,2,3,4,5,6,7,8,9,10};
int ipar=1;
int con1=0,con2=0,con3=0,con4=0;
int order1=4,order2=4;
int iopen1=1,iopen2=1;
SISLSurf *pigPatch=0;
int jstat=0;
// Load points
std::string filename;
std::cout << "Please, enter 121 Grid Points filename: ";
std::getline (std::cin,filename);
std::string line;
std::cout<<"opening "<<filename<<"\n";
std::ifstream ptsfile (filename.c_str());
if (ptsfile.is_open())
{
	int i=0;
	std::vector<std::string> cooFields;
	while ( std::getline (ptsfile,line) )
    {
      	// split line to get coordinates x,y,z
    	cooFields.clear();    	
		boost::split(cooFields,line,boost::is_any_of( "," ));
		double x = boost::lexical_cast<float>(cooFields[0]);
	    double y = boost::lexical_cast<float>(cooFields[1]);
	    double z = boost::lexical_cast<float>(cooFields[2]);
		points[i*3+0]=x;points[i*3+1]=y;points[i*3+2]=z;
		i++;

    }
    ptsfile.close();
  }
else std::cout << "Unable to open file\n"; 

// Fit surface patch
s1537(points, im1, im2, idim, par1, par2, con1, con2, con3, con4, order1, order2, iopen1, iopen2, &pigPatch, &jstat);

// Write surface info and clean up
std::string surfFile,ptsFile;

std::cout << "Please, enter spline surface filename: ";
std::getline (std::cin,surfFile);

std::ofstream os_surf(surfFile.c_str());
if (!os_surf ) {
	throw std::runtime_error("Unable to open output file.");
}
writeGoSurface(pigPatch, os_surf);	    
freeSurf(pigPatch);
os_surf.close();

std::cout << "\nSuccess creating and saving spline surface";
return 0;
}