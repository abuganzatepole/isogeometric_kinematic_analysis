''' 
Isogeometric Shell Kinematics
ABT.
This program implements basic functions to evaluate the deformation of NURBS
surfaces.'''

## NOTE: check and install the relevant Python modules. A good strategy is to start
## with Python distribution such as Anaconda or Canopy. 

import numpy as np
import scipy 
from scipy.misc import comb
from numpy import cos, sin
import copy
from GeomPatch import GeomPatch
from B_spline import B_spline
from NURBS_Curve import NURBS_Curve
from tensorAlgebra import *


class MembranePatch(GeomPatch):
	''' In addition to geometry, stores a displacement field of the surface'''
	def __init__(self,xiKnot = [], etaKnot = [], Pts=[], xiDeg = 2, etaDeg = 2):
		''' constructor is the same as GeomPatch, but with the following changes:
		## 1) Pts: now has extra columns corresponding to displacements and internal var'''
		
		GeomPatch.__init__(self,xiKnot, etaKnot, Pts, xiDeg, etaDeg)

		''' 3U model
		## Pts:
		##  Pts[ii,jj, 0:3]: coordinates of control points
		##  Pts[ii,jj,   3]: weight of control point
		##  Pts[ii,jj,   4]: thickness
		##  Pts[ii,jj, 5:8]: mid-surface displacement dof'''
		self.params = 3 # this is the displacement only membrane, so it takes 3 midsurface displacement as dof
		self.h = Pts[0,0,4] ## assume constant thickness  
		self.nPts = len(Pts[:,0,0])*len(Pts[0,:,0])

	def evalMembraneGeom(self,xi,eta,zeta):
		''' evaluate the shell model at xi,eta,zeta
		## DATA
		##  xi: param value in the Xi knot
		##  eta: param value in the Eta knot 
		##  zeta: param value along normal
		## RESULT:
		##  J_surf: jacobian of map from parameter to midsurface
		##  G_inv: inverse of the undeformed metric
		##  g: deformed metric
		##  R: array with non-zero shape functions, dimension (xiDeg+1)*(etaDeg+1)
		##  dRdxi: array with derivative of shape functions wrt parameter coordinates
		##  F: deformation gradient'''

		## pre-processing
		## get basis functions at point 'k'
		
		[R,dRdxi,d2Rdxi] = self.evalD2R(xi,eta) 
		
		## Surface calculations
		## initialize basis of the tangent space of undeformed manifold (first derivative of surface)
		E1 = np.zeros((3,1)) 
		E2 = np.zeros((3,1))
		## initialize basis of the tangent space of the deformed manifold (first derivative of surface)
		e1 = np.zeros((3,1)) 
		e2 = np.zeros((3,1))
		## initialize second derivative of the undeformed surface
		d2Sdxi    = np.zeros((3,1)) 
		d2Sdeta   = np.zeros((3,1)) 
		d2Sdxieta = np.zeros((3,1))
		## initialize second derivative of the deformed surface
		d2sdxi    = np.zeros((3,1)) 
		d2sdeta   = np.zeros((3,1)) 
		d2sdxieta = np.zeros((3,1))
		## thickness
		h = 0
		for l in range(len(R)):
			E1[0,0] += self.Pts[R[l,1],R[l,2],0]*dRdxi[l,0]
			E1[1,0] += self.Pts[R[l,1],R[l,2],1]*dRdxi[l,0]
			E1[2,0] += self.Pts[R[l,1],R[l,2],2]*dRdxi[l,0]
			E2[0,0] += self.Pts[R[l,1],R[l,2],0]*dRdxi[l,1]
			E2[1,0] += self.Pts[R[l,1],R[l,2],1]*dRdxi[l,1]
			E2[2,0] += self.Pts[R[l,1],R[l,2],2]*dRdxi[l,1]
			e1[0,0] += self.Pts[R[l,1],R[l,2],5]*dRdxi[l,0]
			e1[1,0] += self.Pts[R[l,1],R[l,2],6]*dRdxi[l,0]
			e1[2,0] += self.Pts[R[l,1],R[l,2],7]*dRdxi[l,0]
			e2[0,0] += self.Pts[R[l,1],R[l,2],5]*dRdxi[l,1]
			e2[1,0] += self.Pts[R[l,1],R[l,2],6]*dRdxi[l,1]
			e2[2,0] += self.Pts[R[l,1],R[l,2],7]*dRdxi[l,1]
			d2Sdxi[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,0]
			d2Sdxi[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,0]
			d2Sdxi[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,0]
			d2Sdeta[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,1]
			d2Sdeta[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,1]
			d2Sdeta[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,1]
			d2Sdxieta[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,2]
			d2Sdxieta[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,2]
			d2Sdxieta[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,2]
			d2sdxi[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*d2Rdxi[l,0]
			d2sdxi[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*d2Rdxi[l,0]
			d2sdxi[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*d2Rdxi[l,0]
			d2sdeta[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*d2Rdxi[l,1]
			d2sdeta[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*d2Rdxi[l,1]
			d2sdeta[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*d2Rdxi[l,1]
			d2sdxieta[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*d2Rdxi[l,2]
			d2sdxieta[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*d2Rdxi[l,2]
			d2sdxieta[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*d2Rdxi[l,2]
			h += self.Pts[R[l,1],R[l,2],4]*R[l,0]
		t = zeta*(h/2.)
		J_t = h/2.
		e1 += E1
		e2 += E2
		
		## evaluate derivatives and normals
		[N,dNdE1,dNdE2] = self.evalDNormal(E1,E2)
		[n,dnde1,dnde2] = self.evalDNormal(e1,e2)
		
		## metric at the undeformed surface
		Gs = np.array([[E1[0,0],E2[0,0],N[0,0]],\
					   [E1[1,0],E2[1,0],N[1,0]],\
					   [E1[2,0],E2[2,0],N[2,0]]])
		## jacobian of the surface metric (for integration)
		J_A = np.linalg.det(Gs)
		## metric at the deformed surface
		gs = np.array([[e1[0,0],e2[0,0],n[0,0]],\
					   [e1[1,0],e2[1,0],n[1,0]],\
					   [e1[2,0],e2[2,0],n[2,0]]])

		## Metric tensors in the reference and deformed manifolds
		## Basis at the integration point in the undeformed manifold
		G1 = E1 + t*(np.dot(dNdE1,d2Sdxi)  + np.dot(dNdE2,d2Sdxieta))  
		G2 = E2 + t*(np.dot(dNdE1,d2Sdxieta) + np.dot(dNdE2,d2Sdeta))  
		G3 = copy.deepcopy(N)
		## undeformed metric and its inverse
		G =  np.array([[G1[0,0],G2[0,0],(h/2.)*G3[0,0]],\
					   [G1[1,0],G2[1,0],(h/2.)*G3[1,0]],\
					   [G1[2,0],G2[2,0],(h/2.)*G3[2,0]]])
		G_inv = np.linalg.inv(G)

		## basis at the integration point in the deformed manifold
		g1 = e1 + t*(np.dot(dnde1,d2sdxi)   + np.dot(dnde2,d2sdxieta))  
		g2 = e2 + t*(np.dot(dnde1,d2sdxieta)  + np.dot(dnde2,d2sdeta))  
		g3 = copy.deepcopy(n)
		## deformed metric
		g =  np.array([[g1[0,0],g2[0,0],(h/2.)*g3[0,0]],\
					   [g1[1,0],g2[1,0],(h/2.)*g3[1,0]],\
					   [g1[2,0],g2[2,0],(h/2.)*g3[2,0]]])

		## Deformation gradient
		F = np.outer(g[:,0],G_inv[0,:].reshape(3,1))+\
			np.outer(g[:,1],G_inv[1,:].reshape(3,1))+\
			np.outer(g[:,2],G_inv[2,:].reshape(3,1))
		J_vol = np.linalg.det(F)

		## return packed stuff
		## jacobians, metrics, deformation gradient and other geometry
		geom = [J_A,J_t,J_vol,G,G_inv,g,F,n,t,dnde1,dnde2,d2sdxi,d2sdeta,d2sdxieta]
		## shape functions
		shapeFunc = [R,dRdxi,d2Rdxi]
		return [geom,shapeFunc]
		
	def evalMembraneSurfaceGeom(self,xi,eta):
		''' evaluate the shell model at the midsurface for xi,eta
		## DATA
		##  xi: param value in the Xi knot
		##  eta: param value in the Eta knot '''

		## pre-processing
		## get basis functions at point 'k'
		[R,dRdxi,d2Rdxi] = self.evalD2R(xi,eta) 
		
		## Surface calculations
		## surface position
		Psi = np.zeros((3,1))
		psi = np.zeros((3,1))
		## initialize basis of the tangent space of undeformed manifold (first derivative of surface)
		E1 = np.zeros((3,1)) 
		E2 = np.zeros((3,1))
		## initialize basis of the tangent space of the deformed manifold (first derivative of surface)
		e1 = np.zeros((3,1)) 
		e2 = np.zeros((3,1))
		## initialize second derivative of the undeformed surface
		d2Sdxi    = np.zeros((3,1)) 
		d2Sdeta   = np.zeros((3,1)) 
		d2Sdxieta = np.zeros((3,1))
		## initialize second derivative of the deformed surface
		d2sdxi    = np.zeros((3,1)) 
		d2sdeta   = np.zeros((3,1)) 
		d2sdxieta = np.zeros((3,1))
		## thickness
		h = 0
		for l in range(len(R)):
			Psi[0,0] += self.Pts[R[l,1],R[l,2],0]*R[l,0]
			Psi[1,0] += self.Pts[R[l,1],R[l,2],1]*R[l,0]
			Psi[2,0] += self.Pts[R[l,1],R[l,2],2]*R[l,0]
			psi[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*R[l,0]
			psi[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*R[l,0]
			psi[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*R[l,0]
			E1[0,0] += self.Pts[R[l,1],R[l,2],0]*dRdxi[l,0]
			E1[1,0] += self.Pts[R[l,1],R[l,2],1]*dRdxi[l,0]
			E1[2,0] += self.Pts[R[l,1],R[l,2],2]*dRdxi[l,0]
			E2[0,0] += self.Pts[R[l,1],R[l,2],0]*dRdxi[l,1]
			E2[1,0] += self.Pts[R[l,1],R[l,2],1]*dRdxi[l,1]
			E2[2,0] += self.Pts[R[l,1],R[l,2],2]*dRdxi[l,1]
			e1[0,0] += self.Pts[R[l,1],R[l,2],5]*dRdxi[l,0]
			e1[1,0] += self.Pts[R[l,1],R[l,2],6]*dRdxi[l,0]
			e1[2,0] += self.Pts[R[l,1],R[l,2],7]*dRdxi[l,0]
			e2[0,0] += self.Pts[R[l,1],R[l,2],5]*dRdxi[l,1]
			e2[1,0] += self.Pts[R[l,1],R[l,2],6]*dRdxi[l,1]
			e2[2,0] += self.Pts[R[l,1],R[l,2],7]*dRdxi[l,1]
			d2Sdxi[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,0]
			d2Sdxi[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,0]
			d2Sdxi[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,0]
			d2Sdeta[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,1]
			d2Sdeta[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,1]
			d2Sdeta[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,1]
			d2Sdxieta[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,2]
			d2Sdxieta[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,2]
			d2Sdxieta[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,2]
			d2sdxi[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*d2Rdxi[l,0]
			d2sdxi[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*d2Rdxi[l,0]
			d2sdxi[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*d2Rdxi[l,0]
			d2sdeta[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*d2Rdxi[l,1]
			d2sdeta[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*d2Rdxi[l,1]
			d2sdeta[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*d2Rdxi[l,1]
			d2sdxieta[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*d2Rdxi[l,2]
			d2sdxieta[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*d2Rdxi[l,2]
			d2sdxieta[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*d2Rdxi[l,2]
			h += self.Pts[R[l,1],R[l,2],4]*R[l,0]
		e1 += E1
		e2 += E2
		
		## evaluate derivatives and normals
		[N,dNdE1,dNdE2] = self.evalDNormal(E1,E2)
		[n,dnde1,dnde2] = self.evalDNormal(e1,e2)
		
		## metric at the undeformed surface
		Gs = np.array([[E1[0,0],E2[0,0],N[0,0]],\
					   [E1[1,0],E2[1,0],N[1,0]],\
					   [E1[2,0],E2[2,0],N[2,0]]])
		## metric at the deformed surface
		gs = np.array([[e1[0,0],e2[0,0],n[0,0]],\
					   [e1[1,0],e2[1,0],n[1,0]],\
					   [e1[2,0],e2[2,0],n[2,0]]])

		## return packed stuff
		refGeom = [Psi,E1,E2,N,d2Sdxi,d2Sdeta,d2Sdxieta,Gs,dNdE1,dNdE2]
		defGeom = [psi,e1,e2,n,d2sdxi,d2sdeta,d2sdxieta,gs,dnde1,dnde2]
		shapeFunc = [R,dRdxi,d2Rdxi]
		return [refGeom,defGeom,shapeFunc]

	def evalDNormal(self,E1,E2):
		''' evaluate the normal and its derivatives given the surface tangent base vectors'''
		E1xE2 = cross3(E1,E2);
		nE1xE2 = np.linalg.norm(E1xE2);
		if np.abs(nE1xE2)<1e-10:
			print 'zero normal',E1,E2
		N = (1.0/nE1xE2)*E1xE2
		dNdE1 = (1./nE1xE2)*hodge3(E2) + dyad3((-1./nE1xE2/nE1xE2)*N,np.dot(E1xE2.transpose(),hodge3(E2)))
		dNdE2 = (-1./nE1xE2)*hodge3(E1)+ dyad3((1./nE1xE2/nE1xE2)*N,np.dot(E1xE2.transpose(),hodge3(E1)));
		return N,dNdE1,dNdE2

	
	def evalDeformedMidsurface(self,xi,eta):
		''' evaluate the shell deformed geometry at the point (xi,eta)'''
		Y = np.zeros((3,1))

		## get basis functions at point '(xi,eta)'
		[R,dRdxi,d2Rdxi] = self.evalD2R(xi,eta)
		
		for i in range(len(R)):
			Y[0,0] += (self.Pts[R[i,1],R[i,2],0]+self.Pts[R[i,1],R[i,2],5])*R[i,0]
			Y[1,0] += (self.Pts[R[i,1],R[i,2],1]+self.Pts[R[i,1],R[i,2],6])*R[i,0]
			Y[2,0] += (self.Pts[R[i,1],R[i,2],2]+self.Pts[R[i,1],R[i,2],7])*R[i,0]
		return Y
	
	def evalMetric(self,xi,eta,zeta):
		'''eval mid surface objects:
		G: undeformed metric
		g: deformed metric'''
		[R,dRdxi,d2Rdxi] = self.evalD2R(xi,eta)

		## Surface calculations
		## initialize basis of the tangent space of undeformed manifold (first derivative of surface)
		E1 = np.zeros((3,1)) 
		E2 = np.zeros((3,1))
		d2Sdxi = np.zeros((3,1)) 
		d2Sdeta = np.zeros((3,1))
		d2Sdxieta = np.zeros((3,1))
		## initialize basis of the tangent space of the deformed manifold (first derivative of surface)
		e1 = np.zeros((3,1)) 
		e2 = np.zeros((3,1))
		d2sdxi = np.zeros((3,1)) 
		d2sdeta = np.zeros((3,1))
		d2sdxieta = np.zeros((3,1))	
		h = 0	
		for l in range(len(R)):
			E1[0,0] += self.Pts[R[l,1],R[l,2],0]*dRdxi[l,0]
			E1[1,0] += self.Pts[R[l,1],R[l,2],1]*dRdxi[l,0]
			E1[2,0] += self.Pts[R[l,1],R[l,2],2]*dRdxi[l,0]
			E2[0,0] += self.Pts[R[l,1],R[l,2],0]*dRdxi[l,1]
			E2[1,0] += self.Pts[R[l,1],R[l,2],1]*dRdxi[l,1]
			E2[2,0] += self.Pts[R[l,1],R[l,2],2]*dRdxi[l,1]
			e1[0,0] += self.Pts[R[l,1],R[l,2],5]*dRdxi[l,0]
			e1[1,0] += self.Pts[R[l,1],R[l,2],6]*dRdxi[l,0]
			e1[2,0] += self.Pts[R[l,1],R[l,2],7]*dRdxi[l,0]
			e2[0,0] += self.Pts[R[l,1],R[l,2],5]*dRdxi[l,1]
			e2[1,0] += self.Pts[R[l,1],R[l,2],6]*dRdxi[l,1]
			e2[2,0] += self.Pts[R[l,1],R[l,2],7]*dRdxi[l,1]
			d2Sdxi[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,0]
			d2Sdxi[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,0]
			d2Sdxi[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,0]
			d2Sdeta[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,1]
			d2Sdeta[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,1]
			d2Sdeta[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,1]
			d2Sdxieta[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,2]
			d2Sdxieta[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,2]
			d2Sdxieta[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,2]
			d2sdxi[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*d2Rdxi[l,0]
			d2sdxi[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*d2Rdxi[l,0]
			d2sdxi[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*d2Rdxi[l,0]
			d2sdeta[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*d2Rdxi[l,1]
			d2sdeta[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*d2Rdxi[l,1]
			d2sdeta[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*d2Rdxi[l,1]
			d2sdxieta[0,0] += (self.Pts[R[l,1],R[l,2],0]+self.Pts[R[l,1],R[l,2],5])*d2Rdxi[l,2]
			d2sdxieta[1,0] += (self.Pts[R[l,1],R[l,2],1]+self.Pts[R[l,1],R[l,2],6])*d2Rdxi[l,2]
			d2sdxieta[2,0] += (self.Pts[R[l,1],R[l,2],2]+self.Pts[R[l,1],R[l,2],7])*d2Rdxi[l,2]
			h += self.Pts[R[l,1],R[l,2],4]*R[l,0]
		t = zeta*(h/2.)
		e1 += E1
		e2 += E2
		
		## evaluate derivatives and normals
		[N,dNdE1,dNdE2] = self.evalDNormal(E1,E2)
		[n,dnde1,dnde2] = self.evalDNormal(e1,e2)
		
		## Metric tensors in the reference and deformed manifolds
		## Basis at the integration point in the undeformed manifold
		G1 = E1 + t*(np.dot(dNdE1,d2Sdxi)  + np.dot(dNdE2,d2Sdxieta))  
		G2 = E2 + t*(np.dot(dNdE1,d2Sdxieta) + np.dot(dNdE2,d2Sdeta))  
		G3 = copy.deepcopy(N)
		## undeformed metric and its inverse
		G =  np.array([[G1[0,0],G2[0,0],(h/2.)*G3[0,0]],\
					   [G1[1,0],G2[1,0],(h/2.)*G3[1,0]],\
					   [G1[2,0],G2[2,0],(h/2.)*G3[2,0]]])
		G_inv = np.linalg.inv(G)

		## basis at the integration point in the deformed manifold
		g1 = e1 + t*(np.dot(dnde1,d2sdxi)   + np.dot(dnde2,d2sdxieta))  
		g2 = e2 + t*(np.dot(dnde1,d2sdxieta)  + np.dot(dnde2,d2sdeta))  
		g3 = copy.deepcopy(n)
		## deformed metric
		g =  np.array([[g1[0,0],g2[0,0],(h/2.)*g3[0,0]],\
					   [g1[1,0],g2[1,0],(h/2.)*g3[1,0]],\
					   [g1[2,0],g2[2,0],(h/2.)*g3[2,0]]])
		return G,g
	
	def eval1FF(self,E1,E2):
		'''eval the First Fundamental Form (1FF) at the midsurface'''
		Gs = np.zeros((2,2))
		Gs[0,0] = np.dot(E1.transpose(),E1)
		Gs[0,1] = np.dot(E1.transpose(),E2)
		Gs[1,0] = np.dot(E2.transpose(),E1)
		Gs[1,1] = np.dot(E2.transpose(),E2)
		return Gs
	
	def eval2FF(self,N,d2Sdxi,d2Sdeta,d2Sdxieta):
		'''eval the Second Fundamental Form (2FF) at the midsurface'''
		Bs = np.zeros((2,2))
		Bs[0,0] = np.dot(d2Sdxi.transpose(),N)
		Bs[0,1] = np.dot(d2Sdxieta.transpose(),N)
		Bs[1,0] = np.dot(d2Sdxieta.transpose(),N)
		Bs[1,1] = np.dot(d2Sdeta.transpose(),N)
		return Bs
	
	def corotationQ(self,G):
		'''returns the corotation matrix given covariant basis vectors G'''
		Q = np.zeros((3,3))
		Q[:,0] = (1./np.linalg.norm(G[:,0]))*G[:,0]
		Q[:,2] = (1./np.linalg.norm(G[:,2]))*G[:,2]
		Q[:,1] = (1./np.linalg.norm(np.cross(Q[:,2],Q[:,0])))*np.cross(Q[:,2],Q[:,0])
		##print Q
		return Q
		
	def evalEpsilon(self,Gs,gs,G):
		'''eval the Membrane Strain (Epsilon) at any point along the normal direction in corotated frame'''
		Q = self.corotationQ(G)
		G_cr = np.dot(Q.transpose(),G)
		Ginv = np.linalg.inv(G_cr)
		Epsilon =0.5*( (gs[0,0]-Gs[0,0])*dyad3(Ginv[0,:].reshape(3,1),Ginv[0,:].reshape(3,1))\
					  +(gs[0,1]-Gs[0,1])*dyad3(Ginv[0,:].reshape(3,1),Ginv[1,:].reshape(3,1))\
					  +(gs[1,0]-Gs[1,0])*dyad3(Ginv[1,:].reshape(3,1),Ginv[0,:].reshape(3,1))\
					  +(gs[1,1]-Gs[1,1])*dyad3(Ginv[1,:].reshape(3,1),Ginv[1,:].reshape(3,1)))
		return Epsilon
	
	def evalKappa(self,Bs,bs,G):
		'''eval Bending Strain (Kappa) at any point along the normal direction, since this
		includes the G which serves as the 'translator' Z to refere strains at the midsurface
		to the neighborhood along the normal'''
		Q = self.corotationQ(G)
		G_cr = np.dot(Q.transpose(),G)
		Ginv = np.linalg.inv(G_cr)
		Kappa =      ( (bs[0,0]-Bs[0,0])*dyad3(Ginv[0,:].reshape(3,1),Ginv[0,:].reshape(3,1))\
					  +(bs[0,1]-Bs[0,1])*dyad3(Ginv[0,:].reshape(3,1),Ginv[1,:].reshape(3,1))\
					  +(bs[1,0]-Bs[1,0])*dyad3(Ginv[1,:].reshape(3,1),Ginv[0,:].reshape(3,1))\
					  +(bs[1,1]-Bs[1,1])*dyad3(Ginv[1,:].reshape(3,1),Ginv[1,:].reshape(3,1)))
		return Kappa

	
	def evalMembraneTheta(self,xi,eta):
		''' eval the area change at xi, eta locations '''
		## evaluate the shell at the point (xi,eta)
		G,g = self.evalMetric(xi,eta,0)
		if np.linalg.det(G)!=0:
			G_inv = np.linalg.inv(G)
		else:
			print('Gs is a singular surface at this point, derivatives vanish at xi=%f,eta=%f'%(xi,eta))
			print('Not possible to calculate deformation in a kink. Modeled as identity')
			G = np.eye(3)
			G_inv = np.eye(3)
			
		## Deformation gradient
		F = np.outer(g[:,0],G_inv[0,:].reshape(3,1))+\
			np.outer(g[:,1],G_inv[1,:].reshape(3,1))+\
			np.outer(g[:,2],G_inv[2,:].reshape(3,1))

		theta = np.linalg.det(F)
		return theta
		
	def evalMembraneStrain(self,xi,eta):
		''' evaluate the green lagrange strain tensor at the point (xi,eta)'''
		
		## Surface calculations
		G,g = self.evalMetric(xi,eta,0)
		if np.linalg.det(G)!=0:
			G_inv = np.linalg.inv(G)
		else:
			print('Gs is singular surface at this point, derivatives vanish at xi=%f,eta=%f'%(xi,eta))
			print('Not possible to calculate deformation in a kink. Modeled as identity')
			G = np.eye(3)
			G_inv = np.eye(3)
		## Deformation gradient
		F = np.outer(g[:,0],G_inv[0,:].reshape(3,1))+\
			np.outer(g[:,1],G_inv[1,:].reshape(3,1))+\
			np.outer(g[:,2],G_inv[2,:].reshape(3,1))
		E = 0.5*(np.dot(F.transpose(),F)-np.eye(3))
		return E

	def exportStrainMeshGrid(self,density):
		'''return the components of strain E in global basis over Mesh Grid'''
		## plot the geometry using a mesh grid
		## exports all the components  in the global basis
		dxi  = (self.xiKnot[-1]-self.xiKnot[0])/float(density)
		deta = (self.etaKnot[-1]-self.etaKnot[0])/float(density)
		[xi,eta] = np.mgrid[self.xiKnot[0]:self.xiKnot[-1]+dxi:dxi,self.etaKnot[0]:self.etaKnot[-1]+deta:deta]
		X = np.zeros((nxi,neta))
		Y = np.zeros((nxi,neta))
		Z = np.zeros((nxi,neta))
		E = np.zeros((nxi,neta,6))
		## loop over the grid
		for i in range(len(xi[:,0])):
			for j in range(len(xi[0,:])):
				E = self.evalMembraneStrain(xi[i,j],eta[i,j])
				deformedMidSurf = self.evalDeformedMidsurface(xi[i,j],eta[i,j])
				X[i,j] = deformedMidSurf[0,0]
				Y[i,j] = deformedMidSurf[1,0]
				Z[i,j] = deformedMidSurf[2,0]
				E[i,j,0] = E[0,0]
				E[i,j,1] = E[1,1]
				E[i,j,2] = E[2,2]
				E[i,j,3] = E[0,1]
				E[i,j,4] = E[0,2]
				E[i,j,5] = E[1,2]
		#mlab_mesh(X,Y,Z)
		## return data to be ploted
		return [X,Y,Z,E]
		
	def exportStrainEigMeshGrid(self,density):
		'''return the eigenvalues of strain over the mesh grid'''
		## plot the geometry using a mesh grid
		## exports all the components in the global basis
		dxi  = (self.xiKnot[-1]-self.xiKnot[0])/float(density)
		deta = (self.etaKnot[-1]-self.etaKnot[0])/float(density)
		[xi,eta] = np.mgrid[self.xiKnot[0]:self.xiKnot[-1]+dxi:dxi,self.etaKnot[0]:self.etaKnot[-1]+deta:deta]
		X = np.zeros((nxi,neta))
		Y = np.zeros((nxi,neta))
		Z = np.zeros((nxi,neta))
		E = np.zeros((nxi,neta,6))
		## loop over the grid
		for i in range(len(xi[:,0])):
			for j in range(len(xi[0,:])):
				E = self.evalMembraneStrain(xi[i,j],eta[i,j])
				deformedMidSurf = self.evalDeformedMidsurface(xi[i,j],eta[i,j])
				X[i,j] = deformedMidSurf[0,0]
				Y[i,j] = deformedMidSurf[1,0]
				Z[i,j] = deformedMidSurf[2,0]
				[eiva,eive]=np.linalg.eig(E)
				E[i,j,0] = eiva[0]
				E[i,j,1] = eiva[1]
				E[i,j,2] = eiva[2]
		#mlab_mesh(X,Y,Z)
		## return data to be ploted
		return [X,Y,Z,E]
		
	def exportThetaMeshGrid(self,density):
		'''export the area change theta over the mesh grid'''
		## plot the geometry using a mesh grid
		## exports area change
		dxi  = (self.xiKnot[-1]-self.xiKnot[0])/float(density)
		deta = (self.etaKnot[-1]-self.etaKnot[0])/float(density)
		[xi,eta] = np.mgrid[self.xiKnot[0]:self.xiKnot[-1]+dxi:dxi,self.etaKnot[0]:self.etaKnot[-1]+deta:deta]
		xi  = xi.transpose()
		eta = eta.transpose()
		nxi = xi.shape[0]
		neta = xi.shape[1]
		X = np.zeros((nxi,neta))
		Y = np.zeros((nxi,neta))
		Z = np.zeros((nxi,neta))
		theta = np.zeros((nxi,neta))
		## loop over the grid
		for i in range(len(xi[:,0])):
			for j in range(len(xi[0,:])):
				theta[i,j] = self.evalMembraneTheta(xi[i,j],eta[i,j])
				deformedMidSurf = self.evalDeformedMidsurface(xi[i,j],eta[i,j])
				X[i,j] = deformedMidSurf[0,0]
				Y[i,j] = deformedMidSurf[1,0]
				Z[i,j] = deformedMidSurf[2,0]
		return [X,Y,Z,theta]
		
	def exportMetricMeshGrid(self,density):
		'''export the metrics G,g over the mesh grid'''
		## plot the geometry using a mesh grid
		## exports area change
		dxi  = (self.xiKnot[-1]-self.xiKnot[0])/float(density)
		deta = (self.etaKnot[-1]-self.etaKnot[0])/float(density)
		[xi,eta] = np.mgrid[self.xiKnot[0]:self.xiKnot[-1]:dxi,self.etaKnot[0]:self.etaKnot[-1]:deta]
		xi  = xi.transpose()
		eta = eta.transpose()
		X = 0.0*copy.deepcopy(xi)
		Y = 0.0*copy.deepcopy(xi)
		Z = 0.0*copy.deepcopy(xi)
		G = np.zeros((X.shape[0],X.shape[1],9))
		g = np.zeros((X.shape[0],X.shape[1],9))
		## loop over the grid
		for i in range(len(xi[:,0])):
			for j in range(len(eta[0,:])):
				Gij,gij = self.evalMetric(xi[i,j],eta[i,j],0)
				G[i,j,0] = Gij[0,0];G[i,j,1] = Gij[0,1];G[i,j,2] = Gij[0,2]
				G[i,j,3] = Gij[1,0];G[i,j,4] = Gij[1,1];G[i,j,5] = Gij[1,2]
				G[i,j,6] = Gij[2,0];G[i,j,7] = Gij[2,1];G[i,j,8] = Gij[2,2]
				g[i,j,0] = gij[0,0];g[i,j,1] = gij[0,1];g[i,j,2] = gij[0,2]
				g[i,j,3] = gij[1,0];g[i,j,4] = gij[1,1];g[i,j,5] = gij[1,2]
				g[i,j,6] = gij[2,0];g[i,j,7] = gij[2,1];g[i,j,8] = gij[2,2]
				deformedMidSurf = self.evalDeformedMidsurface(xi[i,j],eta[i,j])
				X[i,j] = deformedMidSurf[0,0]
				Y[i,j] = deformedMidSurf[1,0]
				Z[i,j] = deformedMidSurf[2,0]
		return [X,Y,Z,xi,eta,G,g]
