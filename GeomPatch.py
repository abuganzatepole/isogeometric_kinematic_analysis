''' 
Geometric Patch
ABT.
This program implements a NURBS surface patch with some analytical geometry functions.'''

## NOTE: check and install the relevant Python modules. A good strategy is to start
## with Python distribution such as Anaconda or Canopy. 


import numpy as np
import scipy 
from scipy.misc import comb
from numpy import cos, sin
import copy
from B_spline import B_spline

class GeomPatch():
	'''NURBS surface patch.
	Surface is defined by:
		xiKnot: xi knot vector
		etaKnot: eta knot vector
		xiDeg: degree of the xi spline
		etaDeg: degree of the eta spline
		Pts: the control point net, a 3dimensional array [xiPt,etaPt,Coord]
			Coord belongs to Rn, so any dimensional surfaces are valid'''
	def __init__(self,xiKnot=[], etaKnot=[], Pts=np.zeros((1,1,1)), xiDeg=2, etaDeg=2):
		self.xiKnot     = xiKnot  ## list, alternative np array
		self.etaKnot    = etaKnot ## list, alternative np array
		self.xiDeg      = xiDeg
		self.etaDeg     = etaDeg
		self.Pts        = Pts     ## multi-d np array: [xiPts , etaPTs , 4 + #fields]
		self.xiSpace    = sorted(list(set(xiKnot)))
		self.etaSpace   = sorted(list(set(etaKnot)))
		self.xiBspline  = B_spline(xiKnot, Pts[:,0,0:1],xiDeg) ## only first b-spline
		self.etaBspline = B_spline(etaKnot,Pts[0,:,0:1],etaDeg) ## only first b-spline
		self.IP         = np.array([[-np.sqrt(3)/3.0,-np.sqrt(3)/3.0,1.],\
						  		    [-np.sqrt(3)/3.0, np.sqrt(3)/3.0,1.],\
						   	        [ np.sqrt(3)/3.0,-np.sqrt(3)/3.0,1.],\
						   	        [ np.sqrt(3)/3.0, np.sqrt(3)/3.0,1.]])

	def calcArea(self):
		'''Calculate area of surface patch'''
		area = 0
		## loop over patch elements
		for i in range(len(self.xiSpace)-1):
			for j in range(len(self.etaSpace)-1):
				## loop over integration points
				for k in range(len(self.IP[:,0])):
					[J_omega,xi,eta] = self.evalIP(self.IP[k,0:2],i,j)
					[R,dRdxi,d2Rdxi,E1,E2,g,A] = self.evalSurf(xi,eta)
					J = np.sqrt(np.linalg.det(g))
					area += self.IP[k,2]*J*J_omega
					## NOTE: notice this requires surface integration points, so
					## even if it is inherited in the Membrane Patch you would have to
					## specify surface integration points before calling this function
		return area

	def addKnots(self,xiNewKnots,etaNewKnots):
		''' add new knots in xi and eta knot vectors.
		 start by adding xi knots for each eta row of control points
		 new matrix of control points with extended xi direction'''
		newPtsXi = np.zeros((len(self.Pts[:,0,0])+len(xiNewKnots),len(self.Pts[0,:,0]),len(self.Pts[0,0,:])))
		for i in range(len(self.Pts[0,:,0])):
			## loop over eta rows of control points
			## define the n-dimensional spline needed for that row of control points
			pts = self.Pts[:,i,:]
			## transform from NURBS to b-spline
			pts[:,0] = np.multiply(pts[:,0],pts[:,3])
			pts[:,1] = np.multiply(pts[:,1],pts[:,3])
			pts[:,2] = np.multiply(pts[:,2],pts[:,3])
			auxSpline = B_spline(self.xiKnot,pts,self.xiDeg)
			auxSpline.addKnots(xiNewKnots)
			## store in the corresponding new matrix of points
			newPtsXi[:,i,:]=auxSpline.pts[:,:]
		## update xi knot vector
		self.xiKnot = auxSpline.knot
		##new matrix with extra space for new eta
		newPtsEta = np.zeros((len(self.Pts[:,0,0])+len(xiNewKnots),len(self.Pts[0,:,0])+len(etaNewKnots),len(self.Pts[0,0,:])))
		for i in range(len(self.Pts[:,0,0])+len(xiNewKnots)):
			## define the n-dimensional spline needed for that row of control points
			pts = newPtsXi[i,:,:]
			auxSpline = B_spline(self.etaKnot,pts,self.etaDeg)
			auxSpline.addKnots(etaNewKnots)
			## store in the corresponding new matrix of points
			pts = auxSpline.pts.copy()
			pts[:,0] = np.divide(pts[:,0],pts[:,3])
			pts[:,1] = np.divide(pts[:,1],pts[:,3])
			pts[:,2] = np.divide(pts[:,2],pts[:,3])
			newPtsEta[i,:,:]=pts[:,:].copy()
		## update eta knot vector
		self.etaKnot = auxSpline.knot
		## store the new matrix
		self.Pts = newPtsEta
		self.xiSpace    = sorted(list(set(self.xiKnot)))
		self.etaSpace   = sorted(list(set(self.etaKnot)))
		self.xiBspline  = B_spline(self.xiKnot, self.Pts[:,0,0:1],self.xiDeg) ## only first b-spline
		self.etaBspline = B_spline(self.etaKnot,self.Pts[0,:,0:1],self.etaDeg) ## only first b-spline
		return 

	def elevDegree(self):
		''' elevate degree in both xi and eta directions
		 start by elevating xi direction for each eta row of control points'''
		for i in range(len(self.Pts[0,:,0])):
			## loop over eta rows of control points
			## define the n-dimensional spline needed for that row of control points
			pts = self.Pts[:,i,:].copy()
			## transform from NURBS to b-spline
			pts[:,0] = np.multiply(pts[:,0],pts[:,3])
			pts[:,1] = np.multiply(pts[:,1],pts[:,3])
			pts[:,2] = np.multiply(pts[:,2],pts[:,3])
			auxSpline = B_spline(self.xiKnot,pts,self.xiDeg)
			auxSpline.elevDegree()
			## store in the corresponding new matrix of points
			if i==0:
				# initialize matrix
				newPtsXi = np.zeros((len(auxSpline.pts),len(self.Pts[0,:,0]),len(self.Pts[0,0,:])))
			newPtsXi[:,i,:]=auxSpline.pts[:,:].copy()
		## update xi knot vector and xi degree
		self.xiKnot = auxSpline.knot.copy()
		self.xiDeg += 1
		## elevate degree for each eta
		for i in range(len(newPtsXi[:,0,0])):
			## define the n-dimensional spline needed for that row of control points
			pts = newPtsXi[i,:,:].copy()
			auxSpline = B_spline(self.etaKnot,pts,self.etaDeg)
			auxSpline.elevDegree()
			## store in the corresponding new matrix of points
			if i==0:
				newPtsEta = np.zeros((  len(newPtsXi[:,0,0]),len(auxSpline.pts),len(self.Pts[0,0,:])  ))
			## convert from spline to NURBS
			pts = auxSpline.pts.copy()
			pts[:,0] = np.divide(pts[:,0],pts[:,3])
			pts[:,1] = np.divide(pts[:,1],pts[:,3])
			pts[:,2] = np.divide(pts[:,2],pts[:,3])
			newPtsEta[i,:,:]=pts[:,:].copy()
		## update eta knot vector
		self.etaKnot = auxSpline.knot.copy()
		self.etaDeg += 1
		## store the new matrix
		self.Pts = newPtsEta.copy()
		self.xiSpace    = sorted(list(set(self.xiKnot)))
		self.etaSpace   = sorted(list(set(self.etaKnot)))
		self.xiBspline  = B_spline(self.xiKnot, self.Pts[:,0,0:1],self.xiDeg) ## only first b-spline
		self.etaBspline = B_spline(self.etaKnot,self.Pts[0,:,0:1],self.etaDeg) ## only first b-spline
		return 

	def plotPointCloud(self,density):
		'''generate point cloud by evaluating the patch with some density in the xi and eta
		directions'''
		## loop over the elements;
		count = 0;
		X = []
		Y = []
		Z = []
		for i in range(len(self.xiSpace)-1):
			for j in range(len(self.etaSpace)-1):
				## loop over the density of points in each elements
				for k in range(density+1):
					for l in range(density+1):
						u = self.xiSpace[i]+ k/float(density)*(self.xiSpace[i+1]-self.xiSpace[i])
						v = self.etaSpace[j]+ l/float(density)*(self.etaSpace[j+1]-self.etaSpace[j])						
						R = self.evalR(u,v)
						X.append(0.)
						Y.append(0.)
						Z.append(0.)
						F.append(0.)
						for o in range((self.xiDeg+1)**2):
							X[count] += self.Pts[R[o,1],R[o,2],0]*R[o,0]
							Y[count] += self.Pts[R[o,1],R[o,2],1]*R[o,0]
							Z[count] += self.Pts[R[o,1],R[o,2],2]*R[o,0]
						count += 1	
		## return data to be ploted
		return [X,Y,Z]

	def plotMeshGrid(self,density):
		''' plot the geometry using a mesh grid'''
		dxi  = (self.xiKnot[-1]-self.xiKnot[0])/float(density)
		deta = (self.etaKnot[-1]-self.etaKnot[0])/float(density)
		[xi,eta] = np.mgrid[self.xiKnot[0]:self.xiKnot[-1]+dxi:dxi,self.etaKnot[0]:self.etaKnot[-1]+deta:deta]
		X = 0.0*copy.deepcopy(xi)
		Y = 0.0*copy.deepcopy(xi)
		Z = 0.0*copy.deepcopy(xi)
		F = 0.0*copy.deepcopy(xi)
		## loop over the grid
		for i in range(len(xi[:,0])):
			for j in range(len(xi[0,:])):
				R = self.evalR(xi[i,j],eta[i,j])
				for k in range(len(R[:,0])):
					X[i,j] += self.Pts[R[k,1],R[k,2],0]*R[k,0]
					Y[i,j] += self.Pts[R[k,1],R[k,2],1]*R[k,0]
					Z[i,j] += self.Pts[R[k,1],R[k,2],2]*R[k,0]
		#mlab_mesh(X,Y,Z)
		## return data to be ploted
		return [X,Y,Z,F]


	def evalIP(self,IP,i,j):
		''' DATA: 
		##	IP: parent domain coordinates of the integration point
		##	i : value of the knot xi in parameter space
		##	j : value of the knot eta in parameter space
		## RESULT:
		##	xi,eta : coordinates in parameter space corresponding to this IP
		##  J_omega: jacobian of the map from parent element to parametric space element'''

		## calc param coordinates from IP coordinates
		xi  = (( self.xiSpace[i+1]- self.xiSpace[i])*IP[0]+  self.xiSpace[i+1]+ self.xiSpace[i])/2
		eta = ((self.etaSpace[j+1]-self.etaSpace[j])*IP[1]+ self.etaSpace[j+1]+self.etaSpace[j])/2
		#print('xi: %f,eta: %f, zeta = %f'%(xi,eta,IP[2]))
		dxidomega = np.zeros((2,2))
		dxidomega[0,0] = (self.xiSpace[i+1] - self.xiSpace[i]) /2.
		dxidomega[1,1] = (self.etaSpace[j+1]- self.etaSpace[j])/2.
		J_omega = np.linalg.det(dxidomega)

		## return results
		return [J_omega,xi,eta]

	def evalR(self,xi,eta):
		''' DATA: 
		##	xi : value of interest in the xi parameter space
		##	eta: value of interest in the eta parameter space
		##	xiKnot : knot vector for xi 
		##	etaKnot: knot vector for eta
		##	xiDeg : degree of polynomials for xi
		## 	etaDeg: degree of polynomials for eta
		##	Pts : control points for xi knot
		##  xiBspline : spline object for xi coordinate
		##  etaBspline: spline object for eta coordinate
		## RESULT:
		##	R : array with basis functions evaluated at (xi,eta)'''

		## preprocessing 
		nen = (self.xiDeg+1)*(self.etaDeg+1) # number of basis functions over this element
		
		## initialize output variables
		R = np.zeros((nen,3)) ## note: last two columns are for the DOF number
		
		## initialize local variables
		xiN = np.zeros((self.xiDeg+1)) # B-spline basis functions for xi knot
		etaN = np.zeros((self.etaDeg+1)) # B-spline basis functions for eta knot
		loc_num = 0 # counter for local shape functions
		ii = jj = 0 # loop counters
		sumtot = 0 # dummy sums for rational derivatives

		## calc univariate B-splines
		xiN   = self.xiBspline.evalBasis(xi)
		etaN  = self.etaBspline.evalBasis(eta)
		print(xiN)
		print(etaN)

		## calc bi-variate shape functions and derivatices by tensor product of B-splines
 		for jj in range(len(etaN)):
			if etaN[jj]==0.:
				continue
			else:
 				for ii in range(len(xiN)):
					if xiN[ii]==0.:
						continue
					else:
 						R[loc_num,0] = xiN[ii]*etaN[jj]*self.Pts[ii,jj,3]
						R[loc_num,1] = ii
						R[loc_num,2] = jj
						sumtot += R[loc_num,0]
						loc_num += 1
		for loc_num in range(nen):
			R[loc_num][0] = R[loc_num][0]/sumtot

		## return results
		return R

	def evalD2R(self,xi,eta):
		''' DATA: 
		##	xi : value of interest in the xi parameter space
		##	eta: value of interest in the eta parameter space
		##	xiKnot : knot vector for xi 
		##	etaKnot: knot vector for eta
		##	xiDeg : degree of polynomials for xi
		## 	etaDeg: degree of polynomials for eta
		##	Pts : control points for xi knot
		##  xiBspline : spline object for xi coordinate
		##  etaBspline: spline object for eta coordinate
		## RESULT:
		##	R : array with basis functions evaluated at (xi,eta)
		##  dRdxi
		##  d2Rdxi'''
		
		## preprocessing 
		nen = (self.xiDeg+1)*(self.etaDeg+1) # number of basis functions over this element
		
		## initialize output variables
		R = np.zeros((nen,3)) ## note: last two columns are for the DOF number
		
		## initialize local variables
		xiN = np.zeros((self.xiDeg+1)) # B-spline basis functions for xi knot
		etaN = np.zeros((self.etaDeg+1)) # B-spline basis functions for eta knot
		dRdxi = np.zeros((nen,4)) # derivative of R wrt parametric coord xi and eta
		d2Rdxi = np.zeros((nen,5)) # second derivative of R wrt dxi^2, deta^2 and dxideta
		ii = 0
		jj = 0
		aa = 0
		bb = 0 # loop counters
		loc_num = 0 # counter for local shape functions
		sumxi  = 0 # dummy sums for rational derivatives
		sumeta = 0 # dummy sums for rational derivatives
		sumtot = 0 # dummy sum for rational function
		sum2xi = 0 # dummy sums for rational second derivatives
		sum2eta =0 # dummy sums for rational second derivatives
		sumxieta=0 # dummy sum for cross derivative in rational function

		## calc univariate B-splines
		xiN   = self.xiBspline.evalBasis(xi)
		etaN  = self.etaBspline.evalBasis(eta)
		xiDN  = self.xiBspline.evalDBasis(xi)
		etaDN = self.etaBspline.evalDBasis(eta)
		xiD2N = self.xiBspline.evalD2Basis(xi)
		etaD2N= self.etaBspline.evalD2Basis(eta)

		## calc bi-variate shape functions, derivatives and second derivatives
		## by tensor product of B-splines
 		for jj in range(len(etaN)):
			if etaN[jj]==0. and etaDN[jj]==0. and etaD2N[jj]==0.:
				continue
			else:
 				for ii in range(len(xiN)):
					if xiN[ii]==0.and xiDN[ii]==0. and xiD2N[ii]==0:
						continue
					else:
 						R[loc_num,0] = xiN[ii]*etaN[jj]*self.Pts[ii,jj,3]
						R[loc_num,1] = ii
						R[loc_num,2] = jj
						sumtot += R[loc_num,0]
						dRdxi[loc_num,0] = xiDN[ii]*etaN[jj]*self.Pts[ii,jj,3]
						sumxi  += dRdxi[loc_num,0]
						dRdxi[loc_num,1] = xiN[ii]*etaDN[jj]*self.Pts[ii,jj,3]
						sumeta += dRdxi[loc_num,1]
						dRdxi[loc_num,2] = ii
						dRdxi[loc_num,3] = jj
						d2Rdxi[loc_num,0] = xiD2N[ii]*etaN[jj]*self.Pts[ii,jj,3]
						sum2xi +=d2Rdxi[loc_num,0]
						d2Rdxi[loc_num,1] = xiN[ii]*etaD2N[jj]*self.Pts[ii,jj,3]
						sum2eta +=d2Rdxi[loc_num,1]
						d2Rdxi[loc_num,2] = xiDN[ii]*etaDN[jj]*self.Pts[ii,jj,3]
						sumxieta +=d2Rdxi[loc_num,2]
						d2Rdxi[loc_num,3] = ii
						d2Rdxi[loc_num,4] = jj
						loc_num += 1
		
		for loc_num in range(nen):			
			d2Rdxi[loc_num,0] = (d2Rdxi[loc_num,0]*sumtot**2 \
								-(2*dRdxi[loc_num,0]*sumxi + R[loc_num,0]*sum2xi)*sumtot\
								+R[loc_num,0]*sumxi**2)/(sumtot**3)
			d2Rdxi[loc_num,1] = (d2Rdxi[loc_num,1]*sumtot**2 \
								-(2*dRdxi[loc_num,1]*sumeta + R[loc_num,0]*sum2eta)*sumtot\
								+R[loc_num,0]*sumeta**2)/(sumtot**3)
			d2Rdxi[loc_num,2] = (d2Rdxi[loc_num,2]*sumtot**2\
								-dRdxi[loc_num,0]*sumeta*sumtot\
								-dRdxi[loc_num,1]*sumxi*sumtot\
								-R[loc_num,0]*sumxieta*sumtot\
								+R[loc_num,0]*sumxi*sumeta)/(sumtot**3)
			dRdxi[loc_num,0] = (dRdxi[loc_num,0]*sumtot - R[loc_num,0]*sumxi) /(sumtot**2)
			dRdxi[loc_num,1] = (dRdxi[loc_num,1]*sumtot - R[loc_num,0]*sumeta)/(sumtot**2)
			R[loc_num,0] = R[loc_num,0]/sumtot
		## return results
		return [R,dRdxi,d2Rdxi]

	def evalSurf(xi,eta):
		'''evalualte the surface at (xi,eta). evaluating the surface implies calculating:
		- basis of the tangent space
		- first fundamental form (intrinsic metric)
		- second fundamental form (curvature tensor)'''
		[R,dRdxi,d2Rdxi] = self.evalD2R(xi,eta)
		## basis of the tangent space
		E1 = np.zeros((3,1)) 
		E2 = np.zeros((3,1))
		## second derivatives of the surface
		d2Sdxi    = np.zeros((3,1)) 
		d2Sdeta   = np.zeros((3,1)) 
		d2Sdxieta = np.zeros((3,1))
		for l in range(len(R)):
			E1[0,0] += self.Pts[R[l,1],R[l,2],0]*dRdxi[l,0]
			E1[1,0] += self.Pts[R[l,1],R[l,2],1]*dRdxi[l,0]
			E1[2,0] += self.Pts[R[l,1],R[l,2],2]*dRdxi[l,0]
			E2[0,0] += self.Pts[R[l,1],R[l,2],0]*dRdxi[l,1]
			E2[1,0] += self.Pts[R[l,1],R[l,2],1]*dRdxi[l,1]
			E2[2,0] += self.Pts[R[l,1],R[l,2],2]*dRdxi[l,1]
			d2Sdxi[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,0]
			d2Sdxi[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,0]
			d2Sdxi[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,0]
			d2Sdeta[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,1]
			d2Sdeta[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,1]
			d2Sdeta[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,1]
			d2Sdxieta[0,0] += self.Pts[R[l,1],R[l,2],0]*d2Rdxi[l,2]
			d2Sdxieta[1,0] += self.Pts[R[l,1],R[l,2],1]*d2Rdxi[l,2]
			d2Sdxieta[2,0] += self.Pts[R[l,1],R[l,2],2]*d2Rdxi[l,2]
		E1xE2 = np.cross(E1,E2,axisa=0,axisb=0).reshape(3,1)
		norm_E1xE2 = np.linalg.norm(np.cross(E1,E2,axisa=0,axisb=0),2)
		## normal to the surface
		N = (E1xE2/norm_E1xE2).reshape(3,1) 
		E3 = N.copy()
		## metric tensor
		G = np.array([[E1[0,0],E2[0,0],E3[0,0]],\
					   [E1[1,0],E2[1,0],E3[1,0]],\
					   [E1[2,0],E2[2,0],E3[2,0]]])
		## first fundamental form
		g = np.dot((G[:,0:2]).transpose(),G[:,0:2])
		## second fundamental form
		A11 = np.dot(d2Sdxi.transpose(),N)[0,0]
		A12 = np.dot(d2Sdxieta.transpose(),N)[0,0]
		A22 = np.dot(d2Sdeta.transpose(),N)[0,0]
		A = np.array([[A11,A12],[A12,A22]])
		return [R,dRdxi,d2Rdxi,E1,E2,g,A]