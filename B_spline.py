''' 
B-spline library
ABT.
This program implements basic B-spline function evaluations.'''

## NOTE: check and install the relevant Python modules. A good strategy is to start
## with Python distribution such as Anaconda or Canopy. 

import numpy as np
import scipy 
from scipy.misc import comb
from numpy import cos, sin
import copy

class B_spline():
	'''univariate B-spline'''
	def __init__(self,knot=[],pts=np.zeros((1)),deg=2):
		self.knot  = knot  ## list or numpy array
		self.pts   = pts   ## np array: [pts , 3]
		self.deg   = deg
		self.space = sorted(list(set(knot)))

	def evalBasis(self,u):
		''' eval basis functions up to the degree of the spline curve'''
		nplusc = len(self.pts[:,0])+self.deg
		temp = np.zeros((nplusc))
		## calc 0 order basis function N_{i,0}
		for i in range(nplusc):
			if(u>=self.knot[i] and u<self.knot[i+1]):
				temp[i]=1.
		## calc higher order basis functions
		d = e = 0.
		for k in range(1,self.deg+1):
			for i in range(nplusc-k):
				if temp[i]!=0.:
					d = ((u - self.knot[i])*temp[i])/(self.knot[i+k]-self.knot[i])
				else:
					d = 0.
				if temp[i+1]!=0.:
					e = ((self.knot[i+k+1]-u)*temp[i+1])/(self.knot[i+k+1]-self.knot[i+1])
				else:
					e = 0.
				temp[i] = d+e
		if(u == self.knot[nplusc-1]):
			temp[len(self.pts[:,0])-1] = 1.
  		N = np.zeros((len(self.pts[:,0]))) ## note: basis functions returned in np array
		for i in range(len(self.pts[:,0])):
			N[i] = temp[i]
		if u == self.knot[nplusc-1]:
			N[len(self.pts[:,0])-1] = 1.
  		return N

	def evalBasisP(self,u,P):
		''' eval basis functions up to degree P'''
		nplusc = len(self.pts[:,0])+self.deg
		temp = np.zeros((nplusc))
		## calc 0 order basis function N_{i,0}
		for i in range(nplusc):
			if(u>=self.knot[i] and u<self.knot[i+1]):
				temp[i]=1.
		## calc higher order basis functions
		d = e = 0.
		for k in range(1,P+1):
			for i in range(nplusc-k):
				if temp[i]!=0.:
					d = ((u - self.knot[i])*temp[i])/(self.knot[i+k]-self.knot[i])
				else:
					d = 0.
				if temp[i+1]!=0.:
					e = ((self.knot[i+k+1]-u)*temp[i+1])/(self.knot[i+k+1]-self.knot[i+1])
				else:
					e = 0.
				temp[i] = d+e
		if(u == self.knot[nplusc-1]):
			temp[len(self.pts[:,0])-1] = 1.
  		N = np.zeros((len(self.pts[:,0]))) 
		for i in range(len(self.pts[:,0])):
			N[i] = temp[i]
		if u == self.knot[nplusc-1]:
			N[len(self.pts[:,0])-1] = 1.
  		return N

	def evalDBasis(self,u):
		'''eval derivative of basis functions '''
		N = self.evalBasisP(u,self.deg-1)
		npts = len(self.pts[:,0])
		dN  = np.zeros((npts)) 
		for i in range(npts):
			a = b = 0
			if (self.knot[i+self.deg]-self.knot[i])==0.:
				a = 0.
			else:
				a = self.deg*N[i]/(self.knot[i+self.deg]-self.knot[i])
			if (self.knot[i+self.deg+1]-self.knot[i+1])==0.:
				b = 0.
			else:
				b = self.deg*N[i+1]/(self.knot[i+self.deg+1]-self.knot[i+1])
			dN[i] = a - b
		return dN

	def evalDBasisP(self,u,P):
		''' evaluate derivative of basis function of degree P'''
		N = self.evalBasisP(u,P-1)
		npts = len(self.pts[:,0])
		dN  = np.zeros((npts)) 
		for i in range(npts):
			a = b = 0
			if (self.knot[i+P]-self.knot[i])==0.:
				a = 0.
			else:
				a = P*N[i]/(self.knot[i+P]-self.knot[i])
			if (self.knot[i+P+1]-self.knot[i+1])==0.:
				b = 0.
			else:
				b = P*N[i+1]/(self.knot[i+P+1]-self.knot[i+1])
			dN[i] = a - b
		return dN
		
	def evalDBasis_kp(self,u,k,p):
		'''evaluate derivative k of basis functions of degree p at point u'''
		## dN_(k,p) needs dN_(k-1,p-1)
		## check if the derivative will be automatically zero
		npts = len(self.pts[:,0])
		if k>p:
			return np.zeros((npts))
		## start from (k,p) = 0,p-k and move up
		dN_kp = self.evalBasisP(u,p-k)
		## move up
		deg = p-k+1
		## if no derivatives are needed then return
		if k==0:
			return dN_kp
		for deriv in range(k):
			for i in range(npts):
				a = b = 0
				if (self.knot[i+deg]-self.knot[i])==0.:
					a = 0.
				else:
					a = self.deg*dN_kp[i]/(self.knot[i+deg]-self.knot[i])
				if (self.knot[i+deg+1]-self.knot[i+1])==0.:
					b = 0.
				else:
					b = deg*dN_kp[i+1]/(self.knot[i+deg+1]-self.knot[i+1])
				dN_kp[i] = a - b
			deg+=1
		return dN_kp

	def evalD2Basis(self,u):
		''' eval second derivative of the basis functions '''
		dN = self.evalDBasisP(u,self.deg-1)
		npts = len(self.pts[:,0])
		d2N  = np.zeros((npts)) 
		if self.deg>1:
			for i in range(npts):
				a = b = 0
				if (self.knot[i+self.deg]-self.knot[i])==0.:
					a = 0.
				else:
					a = self.deg*dN[i]/(self.knot[i+self.deg]-self.knot[i])
				if (self.knot[i+self.deg+1]-self.knot[i+1])==0.:
					b = 0.
				else:
					b = self.deg*dN[i+1]/(self.knot[i+self.deg+1]-self.knot[i+1])
				d2N[i] = a - b
		return d2N
	
	def evalD3Basis(self,u):
		'''eval the third derivative of the basis functions'''
		return self.evalDBasis_kp(u,3,self.deg)

	def addKnots(self,newKnots):
		'''add knots to the knot vector'''
		## from MTU (and bohen algorithm)
		# loop over new knots
		# 1 Find the span of the newKnot
		# 2 Insert knot and generate m+1 control points
		for i in range(len(newKnots)):
			newPts = np.zeros((len(self.pts[:,0])+1,len(self.pts[0,:])))
			newKnot = []
			for j in range(len(self.knot)):
				newKnot.append(self.knot[j])
				if newKnots[i]>= self.knot[j] and newKnots[i]<self.knot[j+1]:
					newKnot.append(newKnots[i])
					break
			for k in range(j+1,len(self.knot)):
				newKnot.append(self.knot[k])
			for k in range(j-self.deg+1):
				newPts[k,:] = self.pts[k,:]
			for k in range(j-self.deg+1,j+1):
				a = (newKnots[i]-self.knot[k])/(self.knot[k+self.deg]-self.knot[k])
				newPts[k,:] = (1-a)*self.pts[k-1,:]+a*self.pts[k,:]
			for k in range(j+1,len(self.pts[:,0])+1):
				newPts[k,:] = self.pts[k-1,:]
			self.pts = newPts
			self.knot = newKnot
			self.space = sorted(list(set(newKnot)))
		return 0

	def refine(self,n):
		'''refine by splitting each not interval in n pieces'''
		## define new knot vector 
		newknots = []
		for i in range(len(self.space)-1):
			## calc knot span over the element
			knotSpan = self.space[i+1]-self.space[i]
			## calc new knots spacing over the interval
			newKnotSpan = float(knotSpan)/float(n)
			for j in range(n):		
				newknots.append(self.space[i]+j*newKnotSpan)
		self.addKnots(newknots)
		return 0	
		
	def elevDegree(self):
		''' elevate the degree of the b-spline
		algorithm A5.9 from the nurbs book
		INPUT
		 n: number of control points
		 p: degree
		 U: knot vector
		 Pw: control points
		OUTPUT
		 nh, Uh, Qw'''
		p = self.deg
		n = len(self.pts[:,0])-1
		U = self.knot
		Pw = self.pts
		Qw = np.zeros((len(Pw[:,0])+len(set(self.knot)),len(Pw[0,:])))
		Uh = np.zeros((len(U)+len(set(self.knot))))
		## arrays used in the middle
		bezalfs = np.zeros((p+2,p+1)) ## coeff for degree elev bezier
		bpts = np.zeros((p+1,len(Pw[0,:]))) ## pth-degree bezier control points
		ebpts = np.zeros((p+2,len(Pw[0,:]))) ## (p+1)th-degree bezier control points
		Nextbpts = np.zeros((p-1,len(Pw[0,:]))) ## lefmost control points of next bezier
		alfs = np.zeros((p-1)) ## knot insertion alphas

		# algorithm starts here
		m = n+p+1
		ph = p+1
		ph2 = ph/2
		## compute bezier degree elevation coefficients	
		bezalfs[0,0] = 1.
		bezalfs[ph,p]=1.
		for i in range(1,ph2+1):
			inv = 1/comb(ph,i)
			mpi = np.minimum(p,i)
			for j in range(np.maximum(0,i-1),mpi+1):
				bezalfs[i,j] = inv*comb(p,j)*comb(1,i-j)
		for i in range(ph2+1,ph):
			mpi = np.minimum(p,i)
			for j in range(np.maximum(0,i-1),mpi+1):
				bezalfs[i,j] = bezalfs[ph-i,p-j]
		mh = ph
		kind = ph+1
		r = -1
		a = p
		b = p+1
		cind = 1
		ua = U[0]
		Qw[0,:]=Pw[0,:]
		for i in range(ph+1):
			Uh[i] = ua
		## init first bezier segment
		for i in range(p+1):
			bpts[i,:]=Pw[i,:]
		while(b<m):
			## big loop thru knot vector
			i=b
			while (b<m and np.abs(U[b]-U[b+1])<0.0001):
				b=b+1
			mul = b-i+1
			mh = mh+mul+1
			ub = U[b]
			oldr = r
			r = p-mul
			## insert knot u(b) r times
			if(oldr>0):
				lbz = (oldr+2)/2
			else:
				lbz = 1
			if (r>0):
				rbz = ph-(r+1)/2
			else:
				rbz = ph
			if (r>0):
				##Insert knot to get bezier segment
				numer = ub-ua
				for k in range(p,mul,-1):
					alfs[k-mul-1] = numer/(U[a+k]-ua)
				for j in range(1,r+1):
					save = r-j
					s = mul+j
					for k in range(p,s-1,-1):
						bpts[k,:] = alfs[k-s]*bpts[k,:]+(1-alfs[k-s])*bpts[k-1,:]
					Nextbpts[save,:]=bpts[p,:]
			## end of insert knot
			for i in range(lbz,ph+1):
				## degree elevate bezier
				## only points lbz,...,ph are used below
				ebpts[i,:]=0.*ebpts[i,:]
				mpi = np.minimum(p,i)
				for j in range(np.maximum(0,i-1),mpi+1):
					ebpts[i,:] = ebpts[i,:] + bezalfs[i,j]*bpts[j,:]
			## end of degree elevate bezier
			if oldr>1:
				## must remove knot u = U[a] oldr times
				first = kind-2
				last = kind
				den = ub-ua
				bet = (ub-Uh[kind-1])/den
				for tr in range(1,oldr):
					## knot removal loop
					i = first
					j = last
					kj = j-kind+1
					while (j-i)>tr:
						## loop and compute new control points for one removal step
						if i<cind:
							alf = (ub-Uh[i])/(ua-Uh[i])
							Qw[i,:] = alf*Qw[i,:] + (1-alf)*Qw[i-1,:]
						if j>= lbz:
							if j-tr <= kind-ph+oldr:
								gam = (ub-Uh[j-tr])/den
								ebpts[kj,:] = gam*ebpts[kj,:]+(1-gam)*ebpts[kj+1,:]
							else:
								ebpts[kj,:] = bet*ebpts[kj,:]+(1-bet)*ebpts[kj+1,:]
						i = i+1
						j = j-1
						kj = kj-1
					first = first -1
					last = last+1
			## end of removing knot
			if a!=p:
				## load the knot ua
				for i in range(ph-oldr):
					Uh[kind] = ua
					kind = kind+1
			## check this indentation
			for j in range(lbz,rbz+1):
				## load ctrl pts into Qw
				Qw[cind,:] = ebpts[j,:]
				cind = cind+1
			if b<m:
				for j in range(r):
					bpts[j,:] = Nextbpts[j,:]
				for j in range(r,p+1):
					bpts[j,:] = Pw[b-p+j,:]
				a = b
				b = b+1
				ua = ub
			else:
				## end knot
				for i in range(ph+1):
					Uh[kind+i] = ub
		## end of while loop b<m
		nh = mh-ph-1
		## the end
		self.pts = Qw[:nh+1]
		self.deg += 1
		self.knot = Uh
		self.space = sorted(list(set(Uh)))
		

	def evalSpline(self,u):
		'''eval spline at given value of the parametric coordinate u'''
		N = self.evalBasis(u)
		evaluation = 0*self.pts[0,:]
		for i in range(len(N)):
			evaluation += N[i]*self.pts[i,:] 
		return evaluation

	def evalDSpline(self,u):
		'''eval spline at given value of the parametric coordinate u'''
		N = self.evalDBasis(u)
		evaluation = 0*self.pts[0,:]
		for i in range(len(N)):
			evaluation += N[i]*self.pts[i,:] 
		return evaluation

	def evalD2Spline(self,u):
		'''eval spline at given value of the parametric coordinate u'''
		N = self.evalD2Basis(u)
		evaluation = 0*self.pts[0,:]
		for i in range(len(N)):
			evaluation += N[i]*self.pts[i,:] 
		return evaluation