'''
scale points 
Call main function with three arguments:
	1) 121 grid points filename
	2) tape measure No 1
	3) tape measure No 2
The function outputs
	1) the error and scale corresponding to the tape measures
	2) saves the file with the 121 scaled points
'''

import numpy as np
import sys
from NURBS_Curve import NURBS_Curve

'''load points stored by columns'''
def loadPtsCol(filename):
	lines = open(filename,'r').readlines()
	pts = np.zeros((len(lines),3))
	for i in range(len(pts)):
		cooFields = lines[i].split(',')
		pts[i,0]=float(cooFields[0])
		pts[i,1]=float(cooFields[1])
		pts[i,2]=float(cooFields[2])
	return pts
	
'''read a pline in G2 format and create a curve out of it using the module NURBS_Curve'''
def readSplineCurveG2(splineFile):
	## read in file in G2 format
	splineLines = open(splineFile,'r').readlines()
	## dimension
	dim=3
	## number of points and degree
	aux = splineLines[2].split(' ')
	npts = int(aux[0])
	deg = int(aux[1])-1
	## knot vector
	aux = splineLines[3].split(' ')
	knot = np.zeros((len(aux)-1))
	for i in range(len(aux)-1):
		knot[i] = float(aux[i])
	## points
	pts = np.zeros((npts,dim+1))
	cont=0
	aux = splineLines[4].split(' ')
	for i in range(npts):
		for j in range(dim):
			pts[i,j] = float(aux[cont])
			pts[i,3]=1.
			cont+=1
	## create spline object
	return NURBS_Curve(knot,pts,deg)

'''main() to test the model accuracy'''	
def main_ruler(rulerFiles):
	## open ruler files
	print 'number of rulers: ',len(rulerFiles)
	errors = np.zeros((len(rulerFiles)))
	scales = np.zeros((len(rulerFiles)))
	for r in range(len(rulerFiles)):
		print('Analyzing ruler %i'%r)
		ruler = readSplineCurveG2(rulerFiles[r])
		## store original knot span
		coarseKnot = ruler.knot
		coarseSpace = ruler.space
		## subdivide ruler ten times 
		ruler.refine(10)
		## compute total length
		rulerLength = ruler.calcLength(ruler.knot[0],ruler.knot[-1])
		## compute length between two consecutive points
		sections = np.zeros((len(coarseSpace)-1))
		sectionCount=0
		for i in range(len(coarseSpace)-1):
			sections[sectionCount] = ruler.calcLength(coarseSpace[i],coarseSpace[i+1])
			print('section [%f.%f]: %f'%(coarseSpace[i],coarseSpace[i+1],sections[sectionCount]))
			sectionCount+=1
		## scale
		scale = 0.
		for i in range(len(sections)):
			if i==0 or i==len(sections)-1:
				scale+=sections[i]/2.
			else:
				scale+=sections[i]
		scale = scale/len(sections)
		## error
		error = 0.
		for i in range(len(sections)):
			if i==0 or i==len(sections)-1:
				error+=np.abs(sections[i]/2.-scale)
			else:
				error+=np.abs(sections[i]-scale)
		error = error/len(sections)
		print('\nerror: %f'%error)
		print('scale: %f\n'%scale)
		errors[r] = error
		scales[r] = scale
	print 'errors:\n',errors
	print 'scales:\n',scales
	## scale: based on ruler analysis
	error_avg = np.mean(errors)
	confidence_avg = 1-error_avg
	error_n = len(errors)
	weights = np.zeros((error_n))
	scale = 0
	for i in range(error_n):
		confidence = 1.-errors[i]
		weights[i] = (1.0/error_n)*(1+(confidence - confidence_avg)/confidence_avg)
		scale += weights[i]*scales[i]
	## calculate final error based on the final scale
	error_avg_adjusted = 0.
	for i in range(error_n):
		error_avg_adjusted += np.abs(scales[i]-scale)/scale
	error_avg_adjusted = error_avg_adjusted/error_n
	print 'error average: ',error_avg
	print 'final adjusted error: ',error_avg_adjusted
	print 'weights\n',weights
	print 'final scale: ',scale
	return errors,scales

'''main() to translate, rotate and scale the grid points'''
def main_scale(rulerFiles,ptsNetFile):
	ptsNetProcessedFile = ptsNetFile[:-4]+'_scaled.txt'
	ptsfile = open(ptsNetProcessedFile,'w')
	## read in ruler
	errors,scales = main_ruler(rulerFiles)
	print 'errors:\n',errors
	print 'scales:\n',scales
	## read in points
	pts = loadPtsCol(ptsNetFile)
	## scale: based on ruler analysis
	error_avg = np.mean(errors)
	confidence_avg = 1-error_avg
	error_n = len(errors)
	weights = np.zeros((error_n))
	scale = 0
	for i in range(error_n):
		confidence = 1.-errors[i]
		weights[i] = (1.0/error_n)*(1+(confidence - confidence_avg)/confidence_avg)
		scale += weights[i]*scales[i]
	print 'weights\n',weights
	print 'final scale: ',scale
	scale = 1./scale
	for i in range(len(pts[:,0])):
		## scale
		pts[i,0] = pts[i,0]*scale
		pts[i,1] = pts[i,1]*scale
		pts[i,2] = pts[i,2]*scale
		## write
		ptsfile.write('%f,%f,%f\n'%(pts[i,0],pts[i,1],pts[i,2]))
	ptsfile.close()
		
if __name__=='__main__':
	ptsNetFile = sys.argv[1]
	rulerFiles = []
	for i in range(2,len(sys.argv)):
		rulerFiles.append(sys.argv[i])
	main_scale(rulerFiles,ptsNetFile)