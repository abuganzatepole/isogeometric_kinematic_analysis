/* fit spline curve to tape measure points */

/* to compile and generate the executable.
include directories:
	/Path_to_SISL/SISL-master/include	
	/Path_to_BOOST/include
	/Path_to_SISL/SISL-master/examples/streaming/include
SISL libraries:
	/Path_to_SISL/SISL-master/SISL_build/libsisl.a
Source code (in addition to the source code of this program)
	/Path_to_SISL/SISL-master/examples/streaming/src/GoReadWrite.cpp

run this, compile first, for example:
g++ -I/Path_to_SISL/SISL-master/include/ -I/Path_to_BOOST/include -I/Path_to_SISL/SISL-master/examples/streaming/include -L/Path_to_SISL/SISL-master/SISL_build/ -lsisl generateCurve.cpp /Path_to_SISL/SISL-master/examples/streaming/src/GoReadWrite.cpp -o generateCurve
*/

#include "sisl.h"
#include "GoReadWrite.h"

#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <string>
#include <vector>

int main(int argc, char *argv[])
{

// allocate enough space
double epoint[15*3];
int inbpnt=0; // adjust based on how many points there are in the file
int idim=3;
int nptyp[15];
double epar[]={0.,1.,2.,3.,4.,5.,6.,7.,8.,9.,10.,11.,12.,13.,14.};
int icnsta=0;
int icnend=0;
int iopen = 1;
int ik=4;
double astpar = 0.0;
double cendpar = 0.0;
SISLCurve *rc=NULL;
double *gpar = NULL;
int jnbpar = 0;
int jstat=0;
// Load points
std::string filename;
std::cout << "Please, enter Tape Measure Points filename: ";
std::getline (std::cin,filename);
std::string line;
std::cout<<"opening "<<filename<<"\n";
std::ifstream ptsfile (filename);
if (ptsfile.is_open())
{

	int i=0;
	std::vector<std::string> cooFields;
	while ( std::getline (ptsfile,line) )
    {
      	// split line to get x,y,z coordinates
    	cooFields.clear();    	
		boost::split(cooFields,line,boost::is_any_of( "," ));
		double x = boost::lexical_cast<float>(cooFields[0]);
	    double y = boost::lexical_cast<float>(cooFields[1]);
	    double z = boost::lexical_cast<float>(cooFields[2]);
		epoint[i*3+0]=x;epoint[i*3+1]=y;epoint[i*3+2]=z;
		nptyp[i]=1;
		i++;
		inbpnt++; // increase the number of points

    }
    ptsfile.close();
  }
else std::cout << "Unable to open file\n"; 

// Fit spline
s1356(epoint,inbpnt,idim,nptyp,icnsta,icnend,iopen,ik,astpar,
&cendpar,&rc,&gpar,&jnbpar,&jstat);
std::cout<<"interpolation flag: "<<jstat<<"\n";

// Write spline info and clean up
std::string splineFile;

std::cout << "Please, enter Spline filename: ";
std::getline (std::cin,splineFile);

std::ofstream os_spline(splineFile.c_str());

if (!os_spline) {
	throw std::runtime_error("Unable to open output file.");
}
writeGoCurve(rc, os_spline);	    
freeCurve(rc);
os_spline.close();
std::cout << "\nSuccess creating and saving spline";
return 0;
}