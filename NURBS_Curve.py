''' 
NURBS Curve
ABT.
This program implements basic functions to evaluate the geometry of a NURBS curve.'''

## NOTE: check and install the relevant Python modules. A good strategy is to start
## with Python distribution such as Anaconda or Canopy. 

import numpy as np
import scipy 
from scipy.misc import comb
from numpy import cos, sin
import copy
from B_spline import B_spline

class NURBS_Curve(B_spline):
	'''NURBS 3d curve. Has the same methods as B_spline plus differential geometry of curves'''
	def __init__(self, knot = [], pts=np.zeros((1)), deg = 2):
		## constructor is the same as B_spline
		## pts includes weight for NURBS
		B_spline.__init__(self,knot,pts,deg)
		## integration points
		self.IP         = np.array([[-np.sqrt(3/5.),5./9.],\
									[             0,8./9.],\
									[+np.sqrt(3/5.),5./9.]])

	def calcLength(self,ui,uf):
		'''compute length of the curve between two parameter values'''
		## check if ui,uf is on the knot vector and add if necessary
		if len(np.where(self.knot==ui)[0])==0:
			self.addKnots([ui])
		if len(np.where(self.knot==uf)[0])==0:
			self.addKnots([uf])	
		## loop over corresponding curve elements
		elemi = np.where(self.space==ui)[0][0]
		elemf = np.where(self.space==uf)[0][0]
		length=0
		#print('calc length in interval [%i,%i]'%(elemi,elemf))
		for i in range(elemi,elemf):
			## loop over integration points
			for k in range(len(self.IP[:,0])):
				[J_omega,R,dRdu,d2Rdu,d3Rdu,tau,nu,beta,v,k1,k2] = self.evalCurveIP(self.IP[k,0:2],i)
				length += self.IP[k,1]*v*J_omega
		return length
	
	def evalIP(self,IP,i):
		'''evaluate basis function and derivatives at IP
		 DATA: 
			IP: parent domain coordinate of the integration point
			i : value of the knot in parameter space
			knot : knot vector 
			deg : degree of polynomials
			pts : control points 
		RESULT:
			R : array with basis functions evaluated at IP
		 	J_omega : jacobian of the map from parent to parameter domain
		  dRdxi : gradient of R wrt parameter domain
		  d2Rdxi: second derivative of R wrt parameter domain '''

		## preprocessing 
		nen = (self.deg+1) # number of basis functions over this element

		## initialize output variables
		R = np.zeros((nen,2)) ## note: last column is for the control point number
		
		## initialize local variables
		u  = 0 # B-spline coordinate
		dRdu = np.zeros((nen,2)) # derivative of R wrt parametric coord u
		d2Rdu = np.zeros((nen,2)) # second derivative of R 
		d3Rdu = np.zeros((nen,2)) # third derivative of R 
		dudomega = np.zeros((1)) # derivative of parametric coordinate wrt parent coord
		J_omega = 0 # jacobian of the mapping from parameter space to parent domain
		# loop counters
		ii = 0
		aa = 0 
		loc_num = 0 # counter for local shape functions
		sumu = 0  # dummy sum for rational function
		sumdu  = 0  # dummy sums for rational derivatives
		sumd2u = 0  # dummy sums for rational second derivatives
		sumd3u = 0  # dummy sums for rational second derivatives

		## calc param coordinates from IP coordinates
		u  = (( self.space[i+1]- self.space[i])*IP[0]+  self.space[i+1]+ self.space[i])/2.

		## eval univariate B-splines basis functions, their derivatives, and their second derivatives
		uN   = self.evalBasis(u)
		uDN  = self.evalDBasis(u)
		uD2N = self.evalD2Basis(u)
		uD3N = self.evalD3Basis(u)

		## calc NURBS basis functions and their derivatives
 		for ii in range(len(uN)):
			if uN[ii]==0.:
				continue
			else:
 				R[loc_num,0] = uN[ii]*self.pts[ii,3] ## multiply by weight
				R[loc_num,1] = ii ## store corresponding basis function/control point number
				sumu += R[loc_num,0]
				dRdu[loc_num,0] = uDN[ii]*self.pts[ii,3]
				sumdu  += dRdu[loc_num,0]
				dRdu[loc_num,1] = ii
				d2Rdu[loc_num,0] = uD2N[ii]*self.pts[ii,3]
				sumd2u +=d2Rdu[loc_num,0]
				d2Rdu[loc_num,1] = ii
				d3Rdu[loc_num,0] = uD3N[ii]*self.pts[ii,3]
				sumd3u +=d3Rdu[loc_num,0]
				d3Rdu[loc_num,1] = ii
				loc_num += 1
		for loc_num in range(nen):
			d3Rdu[loc_num,0] = (d3Rdu[loc_num,0]*sumu**3 - 3*d2Rdu[loc_num,0]*sumdu*sumu**2 - 3*dRdu[loc_num,0]*sumd2u*sumu**2\
								+1.5*dRdu[loc_num,0]*sumdu**2*sumu + dRdu[loc_num,0]*sumdu*sumd2u*sumu\
								+0.5*R[loc_num,0]*sumdu*sumd2u*sumu - R[loc_num,0]*sumd3u*sumu**2 - (1/6.)*R[loc_num,0]*sumdu**3)/(sumu**4)
			d2Rdu[loc_num,0] = (d2Rdu[loc_num,0]*sumu**2 \
								-2*dRdu[loc_num,0]*sumdu*sumu - R[loc_num,0]*sumd2u*sumu\
								+R[loc_num,0]*sumdu**2)/(sumu**3)
			dRdu[loc_num,0] = (dRdu[loc_num,0]*sumu - R[loc_num,0]*sumdu) /(sumu**2)
			R[loc_num,0] = R[loc_num,0]/sumu

		## Compute gradient of mapping from parent domain to parameter space
		dudomega[0] = (self.space[i+1] - self.space[i]) /2.
		J_omega = dudomega[0]

		## return results
		return [J_omega,R,dRdu,d2Rdu,d3Rdu]
		
	def evalCurveIP(self,IP,i):
		'''evalualte the curve at the IP. evaluating the curve implies calculating:
		- dr/du (velocity vector wrt parametrization u)
		- d2rdu2 (acceleration vector wrt parametrization u)
		- tau,nu,beta (frenet frame)
		- k1,k2 (first and second curvature, or curvature and torsion)'''
		[J_omega,R,dRdu,d2Rdu,d3Rdu] = self.evalIP(IP,i)
		## first, second and third derivative of the curve
		drdu = np.zeros((3,1)) 
		d2rdu = np.zeros((3,1))
		d3rdu = np.zeros((3,1))
		for l in range(len(R)):
			drdu[0,0] += self.pts[R[l,1],0]*dRdu[l,0]
			drdu[1,0] += self.pts[R[l,1],1]*dRdu[l,0]
			drdu[2,0] += self.pts[R[l,1],2]*dRdu[l,0]
			d2rdu[0,0] += self.pts[R[l,1],0]*d2Rdu[l,0]
			d2rdu[1,0] += self.pts[R[l,1],1]*d2Rdu[l,0]
			d2rdu[2,0] += self.pts[R[l,1],2]*d2Rdu[l,0]
			d3rdu[0,0] += self.pts[R[l,1],0]*d3Rdu[l,0]
			d3rdu[1,0] += self.pts[R[l,1],1]*d3Rdu[l,0]
			d3rdu[2,0] += self.pts[R[l,1],2]*d3Rdu[l,0]
		## velocity of the curve
		v = np.linalg.norm(drdu,2)
		## acceleration dvdu
		a = np.tensordot(d2rdu,drdu)/v
		## geodesic curvature vector
		k1_vec = (1/(v**2))*(d2rdu - (a/v)*drdu)
		## geodesic curvature
		k1 = np.linalg.norm(k1_vec,2)
		## frenet frame
		tau = (1/v)*drdu
		nu  = (1/k1)*k1_vec
		betaaux = np.cross(tau[:,0],nu[:,0])
		beta = np.zeros((3,1))
		beta[:,0] = betaaux
		## d3rds
		k2_vec = (1/v)*((-1/(2*v))*a*k1_vec + (1/v**2)*(d3rdu - (a/v)*d2rdu + 0.5*a**2*drdu\
				-np.tensordot(d3rdu,drdu)*drdu - np.tensordot(d2rdu,d2rdu)*drdu) )
		## torsion
		k2 = (-1/k1**2)*(np.dot(np.cross(tau[:,0],k1_vec[:,0]),k2_vec[:,0]))
		return [J_omega,R,dRdu,d2Rdu,d3Rdu,tau,nu,beta,v,k1,k2]