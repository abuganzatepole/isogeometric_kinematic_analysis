#### TENSOR ALGEBRA FUNCTIONS ####


import numpy as np

def cross3(v1,v2):
	## calculate cross product for two 3x1 vectors
	v3 = np.zeros((3,1))
	v3[0,0] = v1[1,0]*v2[2,0] - v1[2,0]*v2[1,0];
	v3[1,0] = v1[2,0]*v2[0,0] - v1[0,0]*v2[2,0];
	v3[2,0] = v1[0,0]*v2[1,0] - v1[1,0]*v2[0,0];
	return v3
	
def hodge3(v):
	## calculate the matrix corresponding to the hodge operation on a vector
	hodgev = np.zeros((3,3))
	hodgev[0,1]=v[2,0];   
	hodgev[0,2]=-1.0*v[1,0];
	hodgev[1,0]=-1.0*v[2,0];
	hodgev[1,2]=v[0,0];
	hodgev[2,0]=v[1,0];   
	hodgev[2,1]=-1.0*v[0,0];
	return hodgev
	
def dyad3(v1,v2):
	return np.outer(v1,v2)
	
def epsilon(i,j,k):
	if(i==j or i==k or j==k):
		return 0.
	if((i==0 and j==1) or (i==2 and j==3) or (i==3 and j==1)):
		return 1.
	else:
		return -1.

def kronDelta(i,j):
	if i==j:
		return 1.
	else:
		return 0.

def voigtM3V3(M):
	V = np.zeros((3,1))
	V[0,0] = M[0,0]
	V[1,0] = M[1,1]
	V[2,0] = M[0,1]
	return V