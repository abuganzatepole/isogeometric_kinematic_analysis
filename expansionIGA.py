''' 
Skin Expansion
ABT.
This program calculates the deformation of a skin patch.'''

## NOTE: Python packages needed. Anaconda distribution would work. 
## I haven't tried Canopy distributions with this code, but it should work as well
## provided all the relevant packages are installed.

import numpy as np
import sys
import MembranePatch as MP
import copy
## for textures you need matplotlib
from scipy.misc import *
from matplotlib import cm
mycolors = cm.jet

## FUNCTION TO PROCESS A REFERENCE AND A DEFORMED GRID
def evaluateMembraneIGA(referenceFile,deformedFile,resolution,theta_max,theta_min,lambdaG1_max,lambdaG1_min,lambdaG2_max,lambdaG2_min,lineSpace,outputFile):
	## read in reference surface
	PatchReferenceFile = open(referenceFile,'r').readlines()

	## dimension
	dim=3
	## xi degree
	aux = PatchReferenceFile[2].split(' ')
	xinpts = int(aux[0])
	xideg = int(aux[1])-1
	## eta degree
	aux = PatchReferenceFile[4].split(' ')
	etanpts = int(aux[0])
	etadeg = int(aux[1])-1
	## xi knot vector
	aux = PatchReferenceFile[3].split(' ')
	xiknot = np.zeros((len(aux)-1))
	for i in range(len(aux)-1):
		xiknot[i] = float(aux[i])
	## eta knot vector
	aux = PatchReferenceFile[5].split(' ')
	etaknot = np.zeros((len(aux)-1))
	for i in range(len(aux)-1):
		etaknot[i] = float(aux[i])
	## points
	pts = np.zeros((xinpts,etanpts,dim+1+1+dim))
	thickness = 0.25
	cont=0
	aux = PatchReferenceFile[6].split(' ')
	## 
	for j in range(etanpts):
		for i in range(xinpts):
			for k in range(dim):
				pts[i,j,k] = float(aux[cont])
				cont+=1
			pts[i,j,3] = 1.
			pts[i,j,4] = thickness

	## create the skin patch object
	skinPatch = MP.MembranePatch(xiknot,etaknot,pts,xideg,etadeg)

	
	## array of deformations
	theteF_array = [] 	# total area deformation
	I4F_array = []		# deformation in longitudinal direction
	I5F_array = []		# deformation in transverse direction

	## read in the deformed surface
	PatchDeformedFile = open(deformedFile,'r').readlines()
	## dimension
	idim=3
	## xi degree
	aux = PatchDeformedFile[2].split(' ')
	ixinpts = int(aux[0])
	ixideg = int(aux[1])-1
	## eta degree
	aux = PatchDeformedFile[4].split(' ')
	ietanpts = int(aux[0])
	ietadeg = int(aux[1])-1
	## xi knot vector
	aux = PatchDeformedFile[3].split(' ')
	ixiknot = np.zeros((len(aux)-1))
	for i in range(len(aux)-1):
		ixiknot[i] = float(aux[i])
	## eta knot vector
	aux = PatchDeformedFile[5].split(' ')
	ietaknot = np.zeros((len(aux)-1))
	for i in range(len(aux)-1):
		ietaknot[i] = float(aux[i])
	## check that matches with respect to the original patch
	if(idim!=dim or ixideg!=xideg or ietadeg!=etadeg or ixinpts!=xinpts or ietanpts!=etanpts or (ixiknot!=xiknot).any() or (ixiknot!=xiknot).any()):
			print('parametrizations are not equal\n ERROR in surface: %s'%argv[2]) 
	
	## UPDATE DISPLACEMENTS OF ORIGINAL PATCH
	ipts = np.zeros((ixinpts,ietanpts,idim))
	cont=0
	aux = PatchDeformedFile[6].split(' ')
	for j in range(ietanpts):
		for i in range(ixinpts):
			for k in range(idim):
				ipts[i,j,k] = float(aux[cont])
				cont+=1
				## displacement between original and deformed surface
				disp = ipts[i,j,k]-pts[i,j,k]
				## save displacement
				skinPatch.Pts[i,j,k+5] = disp
	[X,Y,Z,xi,eta,G_mg,g_mg] = skinPatch.exportMetricMeshGrid(resolution)
	(dimx,dimy)=X.shape
	thetaFmax = 0.0 	# estimate
	thetaFmin = 2.5		# estimate
	I4max = 0.0			# estimate
	I4min = 2.5			# estimate
	I5max = 0.0			# estimate
	I5min = 2.5			# estimate
	I4_mg = np.zeros(X.shape)
	I5_mg = np.zeros(X.shape)
	theta_mg = np.zeros(X.shape)
	myimage0 = np.ndarray(shape=(dimx,dimy,3), dtype='uint8')
	myimage1 = np.ndarray(shape=(dimx,dimy,3), dtype='uint8')
	myimage2 = np.ndarray(shape=(dimx,dimy,3), dtype='uint8')
	for i in range(dimx):
		for j in range(dimy):
			## process: transform to a corrotated frame
			G = np.zeros((3,3))
			G[0,0] = G_mg[i,j,0];G[0,1] = G_mg[i,j,1];G[0,2] = G_mg[i,j,2];
			G[1,0] = G_mg[i,j,3];G[1,1] = G_mg[i,j,4];G[1,2] = G_mg[i,j,5];
			G[2,0] = G_mg[i,j,6];G[2,1] = G_mg[i,j,7];G[2,2] = G_mg[i,j,8];
			g = np.zeros((3,3))
			g[0,0] = g_mg[i,j,0];g[0,1] = g_mg[i,j,1];g[0,2] = g_mg[i,j,2];
			g[1,0] = g_mg[i,j,3];g[1,1] = g_mg[i,j,4];g[1,2] = g_mg[i,j,5];
			g[2,0] = g_mg[i,j,6];g[2,1] = g_mg[i,j,7];g[2,2] = g_mg[i,j,8];
			## construct co-rotation matrix
			Q1 = G[:,0]/np.linalg.norm(G[:,0])
			Q3 = G[:,2]/np.linalg.norm(G[:,2])
			Q2 = np.cross(Q3,Q1)/np.linalg.norm(np.cross(Q3,Q1))
			Q = np.zeros((3,3))
			Q[0,0] = Q1[0]; Q[0,1] = Q2[0]; Q[0,2] = Q3[0];
			Q[1,0] = Q1[1]; Q[1,1] = Q2[1]; Q[1,2] = Q3[1];
			Q[2,0] = Q1[2]; Q[2,1] = Q2[2]; Q[2,2] = Q3[2];
			## calculate deformation gradient and strain and co-rotate
			if np.linalg.det(G)!=0:
				G_inv = np.linalg.inv(G)
			else:
				print('Gs is a singular surface, derivatives vanish at xi=%f,eta=%f'%(xi,eta))
				G = np.eye(3)
				G_inv = np.eye(3)
			## Deformation gradient
			F = np.outer(g[:,0],G_inv[0,:].reshape(3,1))+\
				np.outer(g[:,1],G_inv[1,:].reshape(3,1))+\
				np.outer(g[:,2],G_inv[2,:].reshape(3,1))
			thetaF = np.linalg.det(F)
			## C
			C = np.dot(F.transpose(),F)
			## co-rotate it 
			C_cr = np.dot(Q.transpose(),np.dot(C,Q))
			## get invariant in co-rotated frame
			I4 = np.sqrt(C_cr[0,0])
			## get the invariant orthogonal
			I5 = np.sqrt(C_cr[1,1])
			I4_mg[i,j] = I4
			I5_mg[i,j] = I5
			theta_mg[i,j] = thetaF
			## get max and min
			if thetaF>thetaFmax:
				thetaFmax = thetaF
			if thetaF<thetaFmin:
				thetaFmin = thetaF
			if I4>I4max:
				I4max = I4
			if I4<I4min:
				I4min = I4
			if I5>I5max:
				I5max = I5
			if I5<I5min:
				I5min = I5

	## save the grid image files and the text files
	txtFileNamethetaF = '%s_thetaF.txt'%outputFile
	txtFileNameI4 = '%s_G1.txt'%outputFile
	txtFileNameI5 = '%s_G2.txt'%outputFile
	np.savetxt(txtFileNamethetaF, theta_mg, fmt='%.2f', delimiter=',', newline='\n')
	np.savetxt(txtFileNameI4, I4_mg, fmt='%.2f', delimiter=',', newline='\n')	
	np.savetxt(txtFileNameI5, I5_mg, fmt='%.2f', delimiter=',', newline='\n')
	#np.savetxt('xi.txt', xi, fmt='%.2f', delimiter=',', newline='\n')
	#np.savetxt('eta.txt', eta, fmt='%.2f', delimiter=',', newline='\n')
	for i in range(dimx):
		for j in range(dimy):
			# scale for the contour plot (between 0 and 1)
			thetaF_scaled = (theta_mg[i,j]-theta_min)/(theta_max-theta_min)
			I4_scaled = (I4_mg[i,j]-lambdaG1_min)/(lambdaG1_max-lambdaG1_min)
			I5_scaled = (I5_mg[i,j]-lambdaG2_min)/(lambdaG2_max-lambdaG2_min)
			if i%lineSpace==0 or j%lineSpace==0:
				myimage0[i,j,0]=255
				myimage0[i,j,1]=255
				myimage0[i,j,2]=255
				myimage1[i,j,0]=255
				myimage1[i,j,1]=255
				myimage1[i,j,2]=255
				myimage2[i,j,0]=255
				myimage2[i,j,1]=255
				myimage2[i,j,2]=255
			else:
				mycolor0 = mycolors(thetaF_scaled)
				mycolor1 = mycolors(I4_scaled)
				mycolor2 = mycolors(I5_scaled)
				myimage0[i,j,0]=int(mycolor0[0]*255)
				myimage0[i,j,1]=int(mycolor0[1]*255)
				myimage0[i,j,2]=int(mycolor0[2]*255)
				myimage1[i,j,0]=int(mycolor1[0]*255)
				myimage1[i,j,1]=int(mycolor1[1]*255)
				myimage1[i,j,2]=int(mycolor1[2]*255)		
				myimage2[i,j,0]=int(mycolor2[0]*255)
				myimage2[i,j,1]=int(mycolor2[1]*255)
				myimage2[i,j,2]=int(mycolor2[2]*255)
	## mark the corners
	myimage0[0,0,0]=0; myimage0[0,0,1]=0; myimage0[0,0,2]=0
	myimage1[0,0,0]=0; myimage1[0,0,1]=0; myimage1[0,0,2]=0
	myimage2[0,0,0]=0; myimage2[0,0,1]=0; myimage2[0,0,2]=0
	myimage0[0,-1,0]=255; myimage0[0,-1,1]=0; myimage0[0,-1,2]=0
	myimage1[0,-1,0]=255; myimage1[0,-1,1]=0; myimage1[0,-1,2]=0
	myimage2[0,-1,0]=255; myimage2[0,-1,1]=0; myimage2[0,-1,2]=0
	myimage0[-1,0,0]=0; myimage0[-1,0,1]=255; myimage0[-1,0,2]=0
	myimage1[-1,0,0]=0; myimage1[-1,0,1]=255; myimage1[-1,0,2]=0
	myimage2[-1,0,0]=0; myimage2[-1,0,1]=255; myimage2[-1,0,2]=0
	myimage0[-1,-1,0]=0; myimage0[-1,-1,1]=0; myimage0[-1,-1,2]=255
	myimage1[-1,-1,0]=0; myimage1[-1,-1,1]=0; myimage1[-1,-1,2]=255
	myimage2[-1,-1,0]=0; myimage2[-1,-1,1]=0; myimage2[-1,-1,2]=255

	
	imFileNamethetaF = '%s_thetaF.png'%outputFile
	imFileNameI4 = '%s_G1.png'%outputFile
	imFileNameI5 = '%s_G2.png'%outputFile
	print 'theta_min',thetaFmin
	print 'theta_max',thetaFmax
	print 'theta_mean',np.mean(theta_mg)
	print 'lambdaG1_min',I4min
	print 'lambdaG1_max',I4max
	print 'lambdaG1_mean',np.mean(I4_mg)
	print 'lambdaG2_min',I5min
	print 'lambdaG2_max',I5max
	print 'lambdaG2_mean',np.mean(I5_mg)
	imsave(imFileNamethetaF,myimage0)
	imsave(imFileNameI4,myimage1)
	imsave(imFileNameI5,myimage2)
	
if __name__=='__main__':
	referenceFile = sys.argv[1]
	deformedFile =  sys.argv[2]
	resolution = float(sys.argv[3])
	theta_max = float(sys.argv[4])
	theta_min = float(sys.argv[5])
	lambdaG1_max = float(sys.argv[6])
	lambdaG1_min = float(sys.argv[7])
	lambdaG2_max = float(sys.argv[8])
	lambdaG2_min = float(sys.argv[9])
	lineSpace = float(sys.argv[10])
	outputFile = sys.argv[11]
	evaluateMembraneIGA(referenceFile,deformedFile,resolution,theta_max,theta_min,lambdaG1_max,lambdaG1_min,lambdaG2_max,lambdaG2_min,lineSpace,outputFile)